import React from 'react';
import Svg, { Rect } from 'react-native-svg';

export default function DiscountIcon() {
  return (
    <Svg width="317" height="58" viewBox="0 0 317 58" fill="none" >
      <Rect x="1" y="1" width="315" height="56" rx="14" fill="white" stroke="#EAEAEA" />
    </Svg>
  );
}

{/* < svg width = "317" height = "58" viewBox = "0 0 317 58" fill = "none" xmlns = "http://www.w3.org/2000/svg" >
</svg > */}