import { Icon } from '@rneui/themed';
import { LinearGradient } from 'expo-linear-gradient'
import React from 'react'
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native'
import Searchbar from '../../components/Searchbar/Searchbar';
import HeartSvg from '../../svgs/searchfilters';
import Pill from '../../components/Pill/Pill';
import CoffeePillSlider from '../../components/CoffeePillSLider/CoffeePillSlider';
import CoffeeCardListing from '../../components/CoffeePillSLider/CoffeeCardListing';

const Home = () => {
    return (
        <View style={{ marginTop: '10%', height: '100%', width: '100%' }}>
            {/* header */}
            <LinearGradient
                colors={['#131313', '#313131']} // Define gradient colors
                start={{ x: 0, y: 0.5 }} // Gradient start point (243 degrees)
                end={{ x: 1, y: 0.5 }} // Gradient end point (243 degrees)
                style={styles.container}
            >
                <View style={styles.headerWrapper}>
                    <View style={{ flexDirection: 'column' }}>
                        <Text style={[styles.headerText, { fontSize: 13 }]}>Location</Text>
                        <Text style={styles.headerText}>Pune, Maharashtra  </Text>
                    </View>
                    <View>
                        <Image source={require('../../../assets/Images/dp.png')} style={styles.dp} />
                    </View>
                </View>
                <View style={{ marginTop: '7.5%', width: '80%', alignSelf: 'center' }}>
                    <Searchbar />
                </View>
            </LinearGradient>
            {/* Banner */}
            <View style={styles.cmsContainer}>
                <Image source={require('../../../assets/Images/cmsblock.png')}
                    style={styles.backgroundImage} />
                <View style={{ width: '80%', alignSelf: 'center' }}>

                    <View style={{ opacity: 1, width: 200, maxWidth: 200, top: -120, }}>
                        <View style={styles.promopill}>
                            <Text style={{ color: '#FFF', fontSize: 14, alignSelf: 'center', fontWeight: '600' }}>Promo</Text>
                        </View>
                        <Text style={styles.cmsText}>Buy one get one Free</Text>
                    </View>
                </View>
            </View>
            {/* Pills and content */}
            <CoffeePillSlider />
            <ScrollView style={{ marginLeft: '10%', height: 800, marginBottom: 50 }}>
                <CoffeeCardListing selectedIndex={0} product={null} />
            </ScrollView>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        height: '35%'
    },
    headerWrapper: {
        flexDirection: 'row',
        marginTop: '10%',
        width: '80%',
        alignSelf: 'center',
    },
    headerText: {
        color: '#DDD',
        fontSize: 14,
    },
    dp: {
        height: 44,
        width: 44,
        marginLeft: '60%'
    },
    cmsContainer: {
        width: 350,
        height: 150,
        alignSelf: 'center',
        top: '-7.5%',
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'stretch', // You can use 'cover', 'contain', 'stretch', etc.
        borderRadius: 20,
        width: '100%',
        height: 150,
        minHeight: 150,
    },
    cmsText: {
        color: '#FFF',
        fontSize: 32,
        fontWeight: '600',
        textShadowColor: 'black',
        textShadowRadius: 30
    },
    promopill: {
        backgroundColor: '#ED5151',
        width: 60, height: 26,
        top: -10,
        borderRadius: 8,
    }
});

export default Home