import { Image, StyleSheet, Text, View, Linking } from 'react-native'
import React from 'react'
import { List } from 'react-native-paper'
import { Icon } from '@rneui/themed'
import { TouchableOpacity } from 'react-native'

const DeliveryTracker = () => {
    return (
        <View style={{ marginTop: '10%', height: '100%', width: '100%' }}>
            <Image source={require('../../../assets/Images/map.png')} style={{ height: '60%', width: '100%', resizeMode: 'cover' }} />
            <View style={{ backgroundColor: '#FFF', height: '35%' }}>
                <View style={{ backgroundColor: '#EAEAEA', alignSelf: 'center', height: 4, width: 65, marginTop: 10 }} />
                <Text style={styles.timeLeft}>10 minutes left</Text>
                {/* delivery by */}
                <View style={{ flexDirection: 'row', marginTop: 10, alignSelf: 'center' }}>
                    <Text style={{ color: '#808080', fontWeight: '400' }}>Delivery By: </Text>
                    <Text style={{ color: '#303336', fontWeight: '600' }}>John Doe</Text>
                </View>
                {/* progress */}
                <View style={{ flexDirection: 'row', marginTop: 25, marginLeft: 10, alignSelf: 'center' }}>
                    <View style={styles.progressbar} />
                    <View style={styles.progressbar} />
                    <View style={styles.progressbar} />
                    <View style={[styles.progressbar, { backgroundColor: '#DFDFDF' }]} />
                </View>
                {/* Delivered your order */}
                <View style={styles.weDeliverOrder}>
                    <View style={{ width: 50, }}>
                        <List.Icon icon={'motorbike'} color='#C67C4E' style={{ height: 30, marginTop: 15 }} />
                    </View>
                    <View style={{ flexDirection: 'column', width: 200, marginLeft: 20 }}>
                        <Text style={{ color: '#303336', fontWeight: '600' }}>Delivering your order</Text>
                        <Text style={{ color: '#808080', fontWeight: '400', fontSize: 12.5, marginTop: 2 }}>We deliver your goods to you in the shortes possible time.</Text>
                    </View>
                </View>
                {/* Delivery boy detail */}
                <View style={{ flexDirection: 'row', width: 300, marginLeft: 20, marginTop: 20, alignSelf: 'center' }}>
                    <Image source={require('../../../assets/Images/deliveryboy.png')} />
                    <View style={{ flexDirection: 'column', marginLeft: 15 }}>
                        <Text style={{ color: '#303336', fontSize: 14, fontWeight: '600' }}>John Doe</Text>
                        <Text style={{ color: '#808080', fontWeight: '400' }}>Personal Courier</Text>
                    </View>
                    <View style={styles.callContainer}>
                        <TouchableOpacity
                            style={styles.phoneWrapper}
                            onPress={() => Linking.openURL(`tel:12345`)}
                        >
                            <Icon name='call' />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default DeliveryTracker

const styles = StyleSheet.create({
    timeLeft: {
        marginTop: 15,
        alignSelf: 'center',
        marginLeft: 10,
        color: '#303336',
        fontSize: 16,
        fontWeight: '600',
        left: -5
    },
    progressbar: {
        backgroundColor: '#36C07E',
        height: 5,
        width: 75,
        borderRadius: 30,
        marginRight: 10
    },
    weDeliverOrder: {
        marginTop: 15,
        flexDirection: 'row',
        alignSelf: 'center',
        width: 320,
        height: 66,
        borderWidth: 1,
        borderColor: '#EAEAEA',
        borderRadius: 10
    },
    callContainer: {
        width: '55%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    phoneWrapper: {
        borderColor: '#DEDEDE',
        borderRadius: 14,
        borderWidth: 1,
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
    }
});