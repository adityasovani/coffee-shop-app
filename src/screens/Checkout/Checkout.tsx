import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import PageHeader from '../../components/Pageheader/Pageheader';
import ModeSelector from '../../components/Checkout/LocationSelector/ModeSelector';
import AddressSelector from '../../components/Checkout/AddressSelector/AddressSelector';
import CheckoutCoffeeCard from '../../components/Checkout/CheckoutCoffeeCard/CheckoutCoffeeCard';
import DiscountCard from '../../components/Checkout/DiscountCard';
import PaySummary from '../../components/Checkout/PaySummary';
import { Icon } from '@rneui/themed';
import PickupDetail from '../../components/Checkout/PickupDetail';
import { useNavigation } from '@react-navigation/native';
import pageRoutes from '../../constants/PageRoutes';

type Props = {
    coffee: any;
};

const Checkout: React.FC<Props> = () => {

    const [selectedMode, setselectedMode] = useState("Deliver");
    const navigate = useNavigation<any>();

    return (
        <View style={styles.pdpContainer}>
            <View style={styles.pdpContent}>
                <PageHeader headerTitle='Order' showHeart={false} />
                <View style={{ marginTop: 25 }}>

                </View>
                <ModeSelector selectedMode={selectedMode} setselectedMode={setselectedMode} />
                <View style={{ marginTop: 25 }}>
                    {
                        selectedMode === 'Deliver'
                            ? <AddressSelector />
                            : <PickupDetail />
                    }
                    <View style={{ backgroundColor: '#EAEAEA', width: '100%', height: 1.5, marginTop: '7.5%' }} />
                    <View style={{ marginTop: 30 }}>
                        <CheckoutCoffeeCard />
                    </View>
                    <View style={{ width: '100%', backgroundColor: '#F4F4F4', height: 4, marginTop: 30 }} />
                    <View style={{ marginTop: 20 }}>
                        <DiscountCard />
                    </View>
                    <View style={{ marginTop: 15, marginLeft: 15 }}>
                        <PaySummary />
                    </View>
                    <View style={{ marginTop: 20, flexDirection: 'column' }}>
                        <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10 }}>
                            <Icon name='payments' color='#C67C4E' size={25} />
                            <View style={[styles.chip, { marginLeft: 15 }]}>
                                <Text style={{ color: '#FFF', fontSize: 13, fontWeight: '500', alignSelf: 'center' }}>Cash</Text>
                            </View>
                            <Text style={{ marginLeft: 10 }}>$5.53</Text>
                            <View style={{ width: '44%', flexDirection: 'row', justifyContent: 'flex-end' }}>
                                <View style={{ width: 26, height: 26, borderRadius: 90, backgroundColor: '#808080' }}>
                                    <Icon name='more-horiz' color='#F2F2F2' />
                                </View>
                            </View>
                        </View>
                    </View>
                    <TouchableOpacity style={styles.orderbutton}
                        onPress={() => navigate.navigate(pageRoutes.tracker)}>
                        <Text style={{ color: '#FFF', fontSize: 16, fontWeight: '600' }}>Order</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default Checkout;

const styles = StyleSheet.create({
    pdpContainer: {
        backgroundColor: '#F9F9F9',
        height: '100%',
        marginTop: '10%',
    },
    pdpContent: {
        width: '90%',
        alignSelf: 'center',
    },
    chip: { height: 25, width: 55, borderRadius: 20, backgroundColor: '#C67C4E', alignItems: 'center' },
    orderbutton: {
        marginTop: 20, height: 50,
        backgroundColor: '#C67C4E',
        width: '90%',
        alignSelf: 'center',
        borderRadius: 18,
        justifyContent: 'center',
        alignItems: 'center'
    }
})