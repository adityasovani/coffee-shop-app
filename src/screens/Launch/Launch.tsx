import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient';
import { TouchableOpacity } from 'react-native';
import GoogleSVG from '../../svgs/google';
import { useNavigation } from '@react-navigation/native';
import pageRoutes from '../../constants/PageRoutes';

const Launch = () => {

    const navigation = useNavigation<any>();

    return (
        <View style={{ marginTop: '10%', height: '100%' }}>
            <Image source={require('../../../assets/Images/launch-page-image.png')}
                style={{ width: '100%', height: '60%', opacity: 0.9 }} />
            <LinearGradient
                colors={['rgba(0, 0, 0, 0.00)', '#000']} // Define gradient colors
                start={[0, 2]} // Gradient start point
                end={[0, 1]} // Gradient end point
                style={styles.lowerDiv}
            >
                <View style={{ width: '80%', alignSelf: 'center', top: '-5%' }}>
                    <Text style={styles.mainText}>Coffee so good, your taste buds will love it.</Text>
                    <Text style={styles.caption}>The best grain, the finest roast, the powerful flavor.</Text>
                </View>
                <TouchableOpacity style={styles.loginWithGoogleButton} onPress={() => navigation.navigate(pageRoutes.dashboard)}>
                    <View style={{ marginRight: 10 }}>
                        <GoogleSVG />
                    </View>
                    <Text style={styles.googlebtnTxt}>Continue with Google</Text>
                </TouchableOpacity>
            </LinearGradient>
        </View >
    )
};

const styles = StyleSheet.create({
    lowerDiv: {
        backgroundColor: 'black',
        opacity: 0.8,
        height: '40%',
        bottom: 0,
        width: '100%',
        alignSelf: 'center',
    },
    mainText: {
        color: '#FFF',
        textAlign: 'center',
        fontSize: 34,
        fontWeight: '600',
        letterSpacing: 0.3,
    },
    caption: {
        width: '75%',
        marginTop: 17,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#A9A9A9',
        fontSize: 14,
        lineHeight: 21,
        fontWeight: '400',
        letterSpacing: 0.14
    },
    loginWithGoogleButton: {
        flexDirection: 'row',
        marginTop: '4%',
        borderRadius: 10,
        backgroundColor: '#FFF',
        alignItems: 'center',
        alignSelf: 'center',
        width: '75%',
        height: 54,
        justifyContent: 'center'
    },
    googlebtnTxt: {
        color: 'rgba(0, 0, 0, 0.54)',
        fontWeight: '500',
        fontSize: 16
    }
});

export default Launch;