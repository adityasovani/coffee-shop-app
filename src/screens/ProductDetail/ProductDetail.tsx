import React, { useEffect, useState } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import PageHeader from '../../components/Pageheader/Pageheader';
import supabaseHelper from '../../services/supabase-helper/supabase-helper';
import { Button, Icon } from '@rneui/themed';
import { useNavigation } from '@react-navigation/native';
import pageRoutes from '../../constants/PageRoutes';

const ProductDetail = ({ route }) => {

    const [coffee, setcoffee] = useState<any>({});
    const [sizes, setsizes] = useState<any>([]);
    const [selectedSizeIndex, setselectedSizeIndex] = useState(1);
    const navigation = useNavigation<any>();

    const getCoffee = async () => {
        let { data: Coffee, error } = await supabaseHelper
            .from('Coffees')
            .select("*")
            .eq('id', route.params.id);

        setcoffee(Coffee[0]);
        setsizes(Coffee[0].sizes.sizes);
    };

    useEffect(() => {
        getCoffee();
    }, []);


    return (
        <View style={styles.pdpContainer}>
            <View style={styles.pdpContent}>
                <PageHeader headerTitle='Detail' />
                <View style={{ flexDirection: 'column', alignSelf: 'center', marginTop: 25 }}>
                    {/* thumbnail */}
                    <Image source={{ uri: coffee.thumbnail_url }} style={styles.productImage} />
                    {/* Name and caption */}
                    <Text style={styles.productName}>{coffee.name}</Text>
                    <Text style={styles.productCaption}>{coffee.caption}</Text>
                    {/* rating, count, icons */}
                    <View style={{ flexDirection: 'row', marginTop: 15 }}>
                        <View style={{ flexDirection: 'row', }}>
                            <Icon name='star' color='#FBBE21' />
                            <Text style={styles.ratingTxt}>{coffee.rating}</Text>
                            <Text style={styles.count}>(230)</Text>
                        </View>
                        <View style={{ flexDirection: 'row', top: -40, marginLeft: '20%', justifyContent: 'center' }}>
                            <View style={[styles.iconWrapper, { marginRight: 20 }]}>
                                <Image source={require('../../../assets/Images/bean.png')} style={styles.beanIcon} />
                            </View>
                            <View style={[styles.iconWrapper]}>
                                <Image source={require('../../../assets/Images/milk.png')} style={styles.beanIcon} />
                            </View>
                        </View>
                    </View>

                    <View style={{ width: 315, alignSelf: 'center', height: 1, backgroundColor: '#EAEAEA' }} />
                    {/* Description */}
                    <View style={{ marginTop: 20 }}>
                        <Text style={styles.descriptionTitle}>Description</Text>
                        <View style={{ width: '90%' }}>
                            <Text style={[styles.description,]}>
                                {coffee.description}
                                <Text style={styles.readMore}>Read More</Text>
                            </Text>
                        </View>
                    </View>
                    {/* Sizes */}
                    <View style={{ marginTop: 50 }}>
                        <Text style={styles.descriptionTitle}>Size</Text>
                        <View style={{ marginTop: 20, flexDirection: 'row' }}>
                            {sizes.map((size: string, idx: number) => {
                                const isActive = selectedSizeIndex === idx;
                                return <TouchableOpacity
                                    onPress={() => setselectedSizeIndex(idx)}
                                    style={
                                        [styles.sizeBox,
                                        (isActive ? styles.sizeBoxActive : {})]}
                                    key={idx}
                                >
                                    <Text
                                        style={
                                            [styles.sizeText, isActive ? styles.sizeTextActive : {}]
                                        }>{size}
                                    </Text>
                                </TouchableOpacity>
                            })}
                        </View>
                    </View>
                    {/* Footer */}
                    <View style={{ flexDirection: 'row', marginTop: 40, width: '100%' }}>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={{ color: '#9B9B9B', fontSize: 14, fontWeight: '400' }}>Price</Text>
                            <Text style={{ color: '#C67C4E', fontSize: 18, fontWeight: '600' }}>$ {coffee.price}</Text>
                        </View>
                        <View style={{ right: -60 }}>
                            <Button title='Buy Now'
                                buttonStyle={styles.buyButton}
                                onPress={() => navigation.navigate(pageRoutes.checkout)}
                            />
                        </View>
                    </View>
                </View>
            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    pdpContainer: {
        backgroundColor: '#F9F9F9',
        height: '100%',
        marginTop: '10%',
    },
    pdpContent: {
        width: '90%',
        alignSelf: 'center',
    },
    productImage: {
        width: 315,
        height: 226,
        borderRadius: 16
    },
    productName: {
        marginTop: 20,
        color: '#2F2D2C',
        fontSize: 20,
        fontStyle: 'normal',
        fontWeight: '600',
    },
    productCaption: {
        color: '#9B9B9B',
        fontSize: 13,
        fontWeight: '400'
    },
    ratingTxt: {
        marginLeft: 4,
        color: '#2F2D2C',
        fontSize: 16,
        fontWeight: '600'
    },
    count: {
        color: '#808080',
        marginLeft: 4,
        marginTop: 2
    },
    beanIcon: {
        width: 25,
        height: 25
    },
    iconWrapper: {
        backgroundColor: '#FFF0F0',
        height: 50,
        width: 50,
        borderRadius: 14,
        justifyContent: 'center',
        alignItems: 'center'
    },
    descriptionTitle: {
        color: '#2F2D2C',
        fontWeight: '600',
        fontSize: 16
    },
    description: {
        marginTop: 15,
        color: '#9B9B9B',
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 22
    },
    readMore: {
        color: '#C67C4E',
        fontSize: 14,
        fontWeight: '600',
    },
    sizeBox: {
        width: 96,
        height: 43,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
        borderRadius: 12,
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#DEDEDE'
    },
    sizeBoxActive: {
        borderColor: '#C67C4E',
        backgroundColor: '#FFF5EE',
    },
    sizeText: {
        color: '#2F2D2C',
        lineHeight: 22,
        fontWeight: '400',
        fontSize: 14
    },
    sizeTextActive: {
        color: '#C67C4E'
    },
    buyButton: {
        backgroundColor: '#C67C4E',
        borderRadius: 16,
        width: 217,
        height: 55,
    }
});

export default ProductDetail;