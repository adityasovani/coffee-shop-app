import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react'
import pageRoutes from './constants/PageRoutes';
import Launch from './screens/Launch/Launch';
import Home from './screens/Home/Home';
import ProductDetail from './screens/ProductDetail/ProductDetail';
import Checkout from './screens/Checkout/Checkout';
import DeliveryTracker from './screens/Delivery/DeliveryTracker';

const Routes = () => {
    const Stack = createNativeStackNavigator();

    return (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name={pageRoutes.launch} component={Launch} />
            <Stack.Screen name={pageRoutes.dashboard} component={Home} />
            <Stack.Screen name={pageRoutes.pdp} component={ProductDetail} initialParams={{ id: 0 }} />
            <Stack.Screen name={pageRoutes.checkout} component={Checkout} />
            <Stack.Screen name={pageRoutes.tracker} component={DeliveryTracker} />
        </Stack.Navigator>
    )
}

export default Routes;