import React from 'react'
import { Input } from "@rneui/base";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { Image, StyleSheet, View } from 'react-native';
import { TextInput } from 'react-native-paper';
import HeartSvg from '../../svgs/searchfilters';

const Searchbar = () => {
    return (
        <TextInput
            style={styles.inputContainer}
            placeholder='Search Coffee'
            contentStyle={{ color: 'pink' }}
            placeholderTextColor='#989898'
            left={<TextInput.Icon icon={'magnify'} color='#FFF' style={{ height: 45, width: 45 }} />}
            right={<TextInput.Icon icon={'tune'} color='#FFF' style={{ backgroundColor: '#C67C4E', height: 45, width: 45, borderRadius: 10 }} />}
        />
    );
};

const styles = StyleSheet.create({
    rightIconContainerStyle: {
        margin: 2.5,
        borderRadius: 12,
        backgroundColor: "#C67C4E",
        width: 44,
        height: 44,
        alignItems: "center",
        justifyContent: "center"
    },
    inputContainer: {
        width: 300,
        backgroundColor: "#313131",
        borderRadius: 10,
        display: "flex",
        borderBottomColor: 'pink'
    }
})

export default Searchbar;