import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

const Pill = ({ label, active, setselectedIndex, index }) => {
    return (
        <TouchableOpacity
            onPress={() => setselectedIndex(index)}
            style={[styles.promopill, active ? styles.activePill : {}]}>
            <Text style={[styles.label, active ? styles.activeLabel : {}]}>{label}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    promopill: {
        backgroundColor: '#F3F3F3',
        borderWidth: 0.5,
        borderColor: 'grey',
        width: 100, height: 40,
        borderRadius: 12,
        marginRight: 10,
        justifyContent: 'center'
    },
    label: {
        color: '#2F4B4E',
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '600'
    },
    activePill: {
        backgroundColor: '#C67C4E',
    },
    activeLabel: {
        color: '#FFF',
    }
})

export default Pill