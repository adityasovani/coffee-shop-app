import React, { useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import CoffeeCard from '../CoffeeCard/CoffeeCard';
import supabaseHelper from '../../services/supabase-helper/supabase-helper';

const CoffeeCardListing = ({ selectedIndex, product }) => {

    const [products, setproducts] = useState([]);

    const getProducts = async () => {
        let { data: Coffees, error } = await supabaseHelper
            .from('Coffees')
            .select('*');
        setproducts(Coffees);
    };

    useEffect(() => {
        getProducts();
    }, [])

    function sliceIntoChunks(arr: Array<any>, chunkSize: number) {
        const res = [];
        for (let i = 0; i < arr.length; i += chunkSize) {
            const chunk = arr.slice(i, i + chunkSize);
            res.push(chunk);
        }
        return res;
    };

    const handleCardPress = (index: number) => {
        alert(`product ID=${index}`);
    };

    return (
        <View>
            {
                products.length ?
                    sliceIntoChunks(products, 2).map((arr: Array<any>, i: number) =>
                        <View style={{ flexDirection: 'row', marginBottom: 15 }} key={i}>
                            {arr.map((coffee, idx) =>
                                <CoffeeCard {...coffee} key={idx}
                                    onCardPress={handleCardPress} />)
                            }
                        </View>
                    )
                    : <></>
            }
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    item: {
        width: 45, // is 50% of container width
        backgroundColor: 'pink',
        height: 88
    }
})

export default CoffeeCardListing;