import React, { useState } from 'react'
import { ScrollView, View } from 'react-native'
import Pill from '../Pill/Pill'
import CoffeeCardListing from './CoffeeCardListing';

const CoffeePillSlider = () => {

    const [coffeeTypes, setcoffeeTypes] = useState(['Cappuccino', 'Machiato', 'Latte', 'Americano']);
    const [selectedIndex, setselectedIndex] = useState(0);

    return (
        <View style={{ top: '-4%', marginLeft: '10%' }}>
            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                style={{ flexDirection: 'row' }}>
                {coffeeTypes.map((label, key) => (
                    <Pill label={label} key={key}
                        active={key == selectedIndex}
                        setselectedIndex={setselectedIndex}
                        index={key} />
                ))}
            </ScrollView>
        </View>
    )
}

export default CoffeePillSlider;