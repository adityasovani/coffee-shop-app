import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import DiscountIcon from '../../svgs/discount';
import { Icon } from '@rneui/themed';

const DiscountCard = () => {
    return (
        <View style={styles.discountContainer}>
            <Icon name='tag' type='antdesign' color='#C67C4E' />
            <Text style={styles.discountLabel}>1 Discount is applied</Text>
            <View style={{ width: '35%', justifyContent: 'flex-end', flexDirection: 'row' }}>
                <Icon name='right' type='antdesign' color='#000' size={20} />
            </View>
        </View>
    )
}

export default DiscountCard;

const styles = StyleSheet.create({
    discountContainer: {
        borderRadius: 14,
        borderWidth: 1,
        borderColor: '#EAEAEA',
        height: 65,
        width: '95%',
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 20
    },
    discountLabel: {
        color: '#2F2D2C',
        fontWeight: '600',
        fontSize: 14,
        marginLeft: 20
    }
});