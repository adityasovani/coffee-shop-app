import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'

const ModeSelector = ({ selectedMode, setselectedMode }) => {
    return (
        <View style={styles.modeSelectorContainer}>
            <View style={{ flexDirection: 'row', }}>
                <TouchableOpacity
                    style={(selectedMode === 'Deliver') ? styles.activePill : styles.inactivePill}
                    onPress={() => setselectedMode('Deliver')}
                >
                    <Text
                        style={(selectedMode === 'Deliver') ? styles.activePillText : styles.inactivePillText}
                    >
                        Deliver
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={(selectedMode === 'Pick Up') ? styles.activePill : styles.inactivePill}
                    onPress={() => setselectedMode('Pick Up')}
                >
                    <Text
                        style={(selectedMode === 'Pick Up') ? styles.activePillText : styles.inactivePillText}
                    >
                        Pick Up
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default ModeSelector

const styles = StyleSheet.create({
    modeSelectorContainer: {
        height: 50,
        backgroundColor: '#F0F0F0',
        borderRadius: 14,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    activePill: {
        height: 40,
        width: '46.5%',
        borderRadius: 10,
        backgroundColor: '#C67C4E',
        justifyContent: 'center',
        alignItems: 'center',
        gap: 10
    },
    activePillText: {
        color: '#FFF',
        fontSize: 16,
        fontWeight: '600'
    },
    inactivePill: {
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    inactivePillText: {
        color: '#2F2D2C',
        fontWeight: '400',
        fontSize: 15
    }
})