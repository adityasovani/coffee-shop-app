import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'

const CheckoutCoffeeCard = () => {
    return (
        <View style={styles.cardContainer}>
            <Image source={require('../../../../assets/icon.png')} style={{ height: 70, width: 70 }} />
            <View style={{ flexDirection: 'column', marginLeft: 20, }}>
                <Text style={styles.cardName}>Capp</Text>
                <Text style={styles.caption}>w/ chocolate</Text>
            </View>
            <View style={{ width: '40%', justifyContent: 'flex-end', flexDirection: 'row', }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={styles.circle}>
                        <Text style={{ fontSize: 18, fontWeight: '500' }}>-</Text>
                    </View>
                    <View style={{ width: 25, alignItems: 'center', justifyContent: 'center' }}>
                        <Text>1</Text>
                    </View>
                    <View style={styles.circle}>
                        <Text style={{ fontSize: 18, fontWeight: '400' }}>+</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default CheckoutCoffeeCard;

const styles = StyleSheet.create({
    cardContainer: {
        height: 55,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#FFFFF'
    },
    cardName: {
        color: '#2F2D2C',
        fontSize: 16,
        fontWeight: '600',
    },
    caption: {
        color: '#9B9B9B',
        fontWeight: '500',
        fontSize: 13
    },
    circle: {
        height: 30,
        width: 30,
        borderRadius: 90,
        backgroundColor: '#FFF',
        borderColor: '#EAEAEA',
        borderWidth: 1,
        alignItems: 'center'
    }
});