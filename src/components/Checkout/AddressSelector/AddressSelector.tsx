import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { deliveryAddress, shipToName } from '../../../constants/addresses'
import { Icon } from '@rneui/themed'

const AddressSelector = () => {
    return (
        <View style={{ marginLeft: 5 }}>
            <Text style={{ color: '#2F2D2C', fontSize: 16, fontWeight: '600' }}>Delivery Address</Text>
            <Text style={{ marginTop: 9, color: '#303336', fontSize: 14, fontWeight: '600' }}>{shipToName}</Text>
            <Text style={{ marginTop: 9, color: '#808080', fontSize: 14, fontWeight: '400' }}>{deliveryAddress}</Text>
            <View style={{ marginTop: 25, flexDirection: 'row' }}>
                <View style={styles.chip}>
                    <Icon type='antdesign' name='edit' size={20} />
                    <Text style={{ fontSize: 12, alignSelf: 'center', marginLeft: 5 }}>Edit Address</Text>
                </View>
                <View style={[styles.chip, { marginLeft: 10 }]}>
                    <Icon name='note' size={20} />
                    <Text style={{ fontSize: 12, alignSelf: 'center', marginLeft: 5 }}>Add Note</Text>
                </View>
            </View>
        </View>
    )
}

export default AddressSelector

const styles = StyleSheet.create({
    chip: {
        color: '#808080',
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#DEDEDE',
        backgroundColor: '#FFF',
        height: 30,
        width: 140,
        flexDirection: 'row',
    }
});