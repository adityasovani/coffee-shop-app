import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const PaySummary = () => {
    return (
        <View style={styles.paysummaryContainer}>
            <Text style={styles.paymentSummaryTitle}>Payment Summary</Text>
            <View style={styles.price}>
                <Text>Price</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: '75%' }}>
                    <Text>$ 4.53</Text>
                </View>
            </View>
            <View style={[styles.price, { marginTop: 15 }]}>
                <Text>Delivery Fee</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: '61%' }}>
                    <Text style={{ textDecorationLine: 'line-through' }}>$ 2.0</Text>
                    <Text style={{ marginLeft: 7.5 }}>$ 1.0</Text>
                </View>
            </View>
            <View style={{ width: '100%', height: 1, backgroundColor: '#EAEAEA', marginTop: 15 }} />
            <View style={{ marginTop: 15, flexDirection: 'row' }}>
                <Text>Total Payment</Text>
                <View style={{ width: '57%', flexDirection: 'row', justifyContent: 'flex-end' }}>
                    <Text>$5.53</Text>
                </View>
            </View>
        </View>
    )
}

export default PaySummary

const styles = StyleSheet.create({
    paysummaryContainer: {
        flexDirection: 'column'
    },
    paymentSummaryTitle: {
        color: '#2F2D2C',
        fontSize: 16,
        fontWeight: '600',
    },
    price: {
        marginTop: 20,
        flexDirection: 'row'
    }
})