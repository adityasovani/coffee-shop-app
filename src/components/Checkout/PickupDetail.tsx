import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { pickupAddress, pickupName } from '../../constants/addresses'
import { Icon } from '@rneui/themed'

const PickupDetail = () => {
    return (
        <View>
            <Text style={{ color: '#2F2D2C', fontSize: 16, fontWeight: '600' }}>Pickup Address</Text>
            <Text style={{ marginTop: 9, color: '#303336', fontSize: 14, fontWeight: '600' }}>
                {pickupName}
            </Text>
            <Text style={{ marginTop: 9, color: '#808080', fontSize: 14, fontWeight: '400' }}>
                {pickupAddress}
            </Text>
            <TouchableOpacity style={styles.locationBtn}>
                <Icon name='location-pin' />
                <Text>Get directions</Text>
            </TouchableOpacity>
        </View>
    )
}

export default PickupDetail

const styles = StyleSheet.create({
    locationBtn: {
        marginTop: 15,
        flexDirection: 'row',
        height: 30,
        alignItems: 'center',
        width: 150,
        borderRadius: 20,
        borderColor: 'grey',
        borderWidth: 0.5,
        paddingLeft: 7.5,
        marginBottom: -5
    }
})