import { useNavigation } from "@react-navigation/native";
import React, { useState } from "react";
import { StyleSheet, TouchableOpacity, View, Text } from "react-native";
import { Icon } from '@rneui/themed';
import HeartSvg from "../../svgs/searchfilters";

type Props = {
    headerTitle?: string;
    showHeart?: boolean;
    heartPressAction?: Function;
};

const PageHeader: React.FC<Props> = ({ headerTitle, showHeart = false, heartPressAction }) => {

    const navigation = useNavigation<any>();
    const [isHeartClicked, setIsHeartClicked] = useState(false);

    const handleHeartpress = () => {
        if (isHeartClicked) {
            heartPressAction('unfavorite');
        } else {
            heartPressAction('favorite');
        }
        setIsHeartClicked(!isHeartClicked);
    }

    return (
        <View style={{ marginTop: 10, flexDirection: 'row', }} >
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', width: '33%' }}>
                <Icon type='antdesign' color='#2F2D2C' name='left' size={20} />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'center', width: '33%' }}>
                <Text style={styles.appbarTitle}>{headerTitle}</Text>
            </View>
            {showHeart ? <View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: '33%' }}>
                <HeartSvg fill="none" />
            </View> : <></>}

        </View>
    );
};

const styles = StyleSheet.create({
    appbarTitle: {
        fontSize: 18,
        fontWeight: '700',
        color: '#2F2D2C',
        alignSelf: 'center'
    }
})

export default PageHeader;