import { useNavigation } from '@react-navigation/native';
import { Icon } from '@rneui/themed';
import React from 'react'
import { Image, View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import pageRoutes from '../../constants/PageRoutes';

type Props = {
    name: string,
    caption: string,
    price: number,
    rating: number,
    onCardPress: any,
    id: number,
    thumbnail_url: string
};

const CoffeeCard: React.FC<Props> = ({ name, caption, price, rating, id, thumbnail_url }) => {

    const navigation = useNavigation<any>();

    return (
        <TouchableOpacity style={styles.card}
            onPress={() => navigation.navigate(pageRoutes.pdp, {
                id: id
            })}>
            <View style={styles.cardHeader}>
                <Image source={{ uri: thumbnail_url }}
                    style={styles.cardImage} />
                <View style={{ width: '80%', alignSelf: 'center' }}>
                    <View style={{ opacity: 1, width: 200, maxWidth: 200, top: -120, }}>
                        <View style={styles.promopill}>
                            <Icon name='star' color='#FBBE21' />
                            <Text style={{ color: '#FFF', fontSize: 14, alignSelf: 'center', fontWeight: '600' }}>{rating}</Text>
                        </View>
                    </View>
                </View>
            </View>
            <View style={{ marginLeft: 10, marginTop: 15 }}>
                <Text style={styles.cardName}>{name}</Text>
                <Text style={styles.cardCaption}>{caption}</Text>
            </View>
            <View style={{ marginLeft: 10, flexDirection: 'row', marginTop: 7 }}>
                <Text style={styles.price}>$ {price}</Text>
                <Icon name='add' color='#F2F2F2' containerStyle={styles.addButtonWrapper} />
            </View>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    card: {
        borderWidth: 0.5,
        borderColor: 'grey',
        height: 250,
        width: 160,
        borderRadius: 20,
        marginRight: 20
    },
    cardHeader: {
        width: 160,
        height: 150,
    },
    cardImage: {
        flex: 1,
        resizeMode: 'cover',
        borderRadius: 20,
        width: '100%',
        height: 150,
        minHeight: 150,
    },
    promopill: {
        width: 60, height: 26,
        top: -20,
        borderRadius: 8,
        flexDirection: 'row'
    },
    cardName: {
        color: '#2F2D2C',
        marginTop: -7,
        fontSize: 16,
        fontWeight: '600',
    },
    cardCaption: {
        color: '#9B9B9B',
        fontWeight: '500',
        fontSize: 12
    },
    price: {
        color: '#2F4B4E',
        fontSize: 18,
        fontWeight: '600'
    },
    addButtonWrapper: {
        height: 32,
        width: 32,
        borderRadius: 10,
        backgroundColor: '#C67C4E',
        justifyContent: 'center',
        left: 50
    }
});

export default CoffeeCard;