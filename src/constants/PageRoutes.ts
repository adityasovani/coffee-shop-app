const pageRoutes = {
    launch: 'Launch',
    login: 'Login',
    dashboard: 'Dashboard',
    pdp: 'PDP',
    orderDetailsSummary: 'OrderDetailsSummary',
    checkout: 'Checkout',
    tracker: 'DeliveryTracker'
};

export default pageRoutes;