# Install instructions
## About
A coffee shop app built using React Native and Supabase. This app is built with the help of [Expo](https://expo.dev).
## Installation
App requires [Node.js](https://nodejs.org/), NPM and [Expo Go](https://expo.dev/client) mobile app to run.

Install the dependencies and devDependencies and start the server.

```sh
cd coffee-shop
npx expo install
npm run start
```

After build, QR code will be displayed on terminal, scan that QR code in Expo Go app, your app will load on your mobile without any Android/ iOS / ADB dependencies !

# Screenshots

![Alt text](https://gitlab.com/adityasovani/coffee-shop-app/-/raw/master/assets/Screenshots/Screenshot_20230908_093331_Expo%20Go.jpg) ![Alt text](https://gitlab.com/adityasovani/coffee-shop-app/-/raw/master/assets/Screenshots/home.jpg) &nbsp;&nbsp; ![Alt text](https://gitlab.com/adityasovani/coffee-shop-app/-/raw/master/assets/Screenshots/PDP.jpg?ref_type=heads)  ![Alt text](https://gitlab.com/adityasovani/coffee-shop-app/-/raw/master/assets/Screenshots/Checkout.jpg?ref_type=heads) &nbsp; ![Alt text](https://gitlab.com/adityasovani/coffee-shop-app/-/raw/master/assets/Screenshots/tracker.jpg?ref_type=heads) 
